package model;

import annotation.Link;
import controller.FrontServlet;

public class Employe {
    int id;
    String firstname;
    String lastname;
    int age;

    public Employe() {}

    @Link(link = "save")
    public void save() {
        FrontServlet.k.println("\n"+"age :"+this.getAge());
        FrontServlet.k.println("firstname :"+this.getFirstname());
        FrontServlet.k.println("lastname :"+this.getLastname());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
