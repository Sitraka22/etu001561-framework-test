package model;

import java.util.HashMap;

public class ViewModel {
    String jsp;
    HashMap<String, Object> data;

    public ViewModel() {}

    public ViewModel(String jsp, HashMap<String, Object> data) {
        this.jsp = jsp;
        this.data = data;
    }

    public String getJsp() {
        return jsp;
    }

    public void setJsp(String jsp) {
        this.jsp = jsp;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }
}
