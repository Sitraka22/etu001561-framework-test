package model;

import annotation.Link;

import java.util.HashMap;

public class Departement {
    private int id;
    private String nom;

    public Departement() {}

    public Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Link(link = "list-dept")
    public ViewModel list_departement() {
        ViewModel model = new ViewModel();
        Departement[] liste = new Departement[3];
        liste[0] = new Departement(1, "Mecanicien");
        liste[1] = new Departement(2, "chauffeur");
        liste[2] = new Departement(3, "securite");
        HashMap<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < liste.length; i++) {
            map.put("dept"+liste[i].getId(), liste[i]);
        }
        model.setData(map);
        model.setJsp("ListDept.jsp");
        return  model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
