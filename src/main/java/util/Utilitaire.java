package util;

import annotation.Link;
import exception.UrlNotFoundException;
import model.Employe;
import model.ViewModel;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

public class Utilitaire {

    private HashMap<String, Method> map = new HashMap<String, Method>();
    public static int size=0;

    public String retrieveUrlFromRawurl(String extension) throws Exception {
        String[] tab = extension.split("/");
        String url = tab[tab.length-1];
        if (!url.contains(".do")) {
            throw new Exception("Cette extension n'est pas reconnu");
        }
        return url.substring(0, url.lastIndexOf("."));
    }

    public String[] getPackages(String path) {
        File directory = new File(path);
        return directory.list();
    }

    public Class[] getClasses(String path) throws ClassNotFoundException {
        String cls = "";
        String[] packages = getPackages(path);
        Vector<String> list = new Vector<>();
        for (int i = 0; i < packages.length; i++) {
            File classes_in_package = new File(path+"\\"+packages[i]);
            String[] classes = classes_in_package.list();
            if (packages[i].contains(".")==false)
            {
            for (int j = 0; j < classes.length; j++) {
                if(classes[j].endsWith(".class")) {
                    cls = classes[j].substring(0, classes[j].lastIndexOf("."));
                    list.add(packages[i]+"."+cls);
                }
            }
            }
        }
        Class[] classes = new Class[list.size()];
        for (int i = 0; i < classes.length; i++) {
            classes[i] = Class.forName(list.get(i));
        }
        return classes;
    }

    public HashMap hashMappping(String path) throws ClassNotFoundException {
        Class[] classes = getClasses(path);
        for (int i = 0; i < classes.length; i++) {
            Method[] methods = classes[i].getDeclaredMethods();
            for (int j = 0; j < methods.length; j++) {
                if(methods[j].getDeclaredAnnotation(Link.class) == null) continue;
                Link urlAnnotation = methods[j].getAnnotation(Link.class);
                map.put(urlAnnotation.link(), methods[j]);
            }
        }
        return map;
    }

    public boolean alreadyOnMap(String key, HashMap map) {
        if(map.containsKey(key)) return true;
        return false;
    }

    public Class getClassByUrlAnnotation(String path, String url) throws ClassNotFoundException {
        Class[] classes = getClasses(path);
        for (int i = 0; i < classes.length; i++) {
            Method[] methods = classes[i].getDeclaredMethods();
            for (int j = 0; j < methods.length; j++) {
                if(methods[j].getDeclaredAnnotation(Link.class) == null) continue;
                else if(methods[j].getDeclaredAnnotation(Link.class).link().equals(url)) {
                    return classes[i];
                }
            }
        }
        return null;
    }

    public Method getMethodByUrlAnnotation(String path, String url) throws ClassNotFoundException {
        Class[] classes = getClasses(path);
        for (int i = 0; i < classes.length; i++) {
            Method[] methods = classes[i].getDeclaredMethods();
            for (int j = 0; j < methods.length; j++) {
                if(methods[j].getDeclaredAnnotation(Link.class) == null) continue;
                else if(methods[j].getDeclaredAnnotation(Link.class).link().equals(url)) {
                    return methods[j];
                }
            }
        }
        return null;
    }
    public Object instance(Field field,String donne) throws ParseException {
        if (field.getType().getSimpleName().equalsIgnoreCase("date")){
            return Date.valueOf(donne);
        }else if(field.getType().getSimpleName().equalsIgnoreCase("string")){
            return donne;
        }else if(field.getType().getSimpleName().equalsIgnoreCase("boolean")){
            return donne.equalsIgnoreCase("true");
        }else if(field.getType().getSimpleName().equalsIgnoreCase("double")){
            return Double.parseDouble(donne);
        }else if(field.getType().getSimpleName().equalsIgnoreCase("int")){
            return Integer.parseInt(donne);
        }
        return null;
    }

    public Object setValues(String[] name, String[] values, Object instance) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {
        Object object=instance.getClass().getDeclaredConstructor().newInstance();
        Field[]fields=instance.getClass().getDeclaredFields();
        for (Field field : fields){
            field.setAccessible(true);
            for (int i = 0; i < name.length ; i++) {
                if (field.getName().equalsIgnoreCase(name[i])){
                    field.set(object,instance(field,values[i]));
                }
            }
        }
        return object;
    }
    public int size(){
        return size;

    }
    public String[]enumToString(Enumeration<String> name){
        Vector<String>valiny=new Vector<>();
        while (name.hasMoreElements()){
             valiny.add(name.nextElement());
        }
        String[] element=new String[valiny.size()];
        int i=0;
        for (int j = 0; j <element.length ; j++) {
            element[i]="";
        }
        for (String t : valiny){
            element[i]=t;
            i++;
        }
        size= element.length;
        return element;

    }

    public ViewModel invokation(String path, String url, HashMap map,String[] name,String[] values) throws UrlNotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException {
        if(!alreadyOnMap(url, map)){
            throw new UrlNotFoundException("Url not found exception!");
        }
        Class cls = getClassByUrlAnnotation(path, url);
        Object object = cls.getDeclaredConstructor().newInstance();
        Object valeur=setValues(name,values,object);
        Method method = getMethodByUrlAnnotation(path, url);
        ViewModel viewModel=new ViewModel();
        try {
            viewModel = (ViewModel) method.invoke(valeur);//pour les methode qui retourne les viewModel
        }catch (Exception e){
            //viewModel.setJsp("index.jsp");
            method.invoke(valeur);
        }
        return viewModel;
    }
    public String getPath(){
        String path=System.getProperty("user.dir");
        return path;
    }


}
