package controller;

import model.ViewModel;
import util.Utilitaire;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;

@WebServlet(name = "FrontServlet", value = "*.do")
public class FrontServlet extends HttpServlet {
    Utilitaire utilitaire = new Utilitaire();
    String path = "E:\\mety\\framework\\build\\web\\WEB-INF\\classes";
    //URL loc = FrontServlet.class.getProtectionDomain().getCodeSource().getLocation() ;
    //String path = loc.getFile() ;
    ServletContext ctx = null;
    public static PrintWriter k=null;
    @Override
    public void init() throws ServletException {
        ctx = this.getServletContext();
        HashMap<String, Method> map = null;
        try {
            map = utilitaire.hashMappping(path);
        } catch (ClassNotFoundException e) {
        }
        ctx.setAttribute("map", map);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        k= response.getWriter();
        Enumeration<String> name=request.getParameterNames();
        String[]formName= utilitaire.enumToString(name);
        String[]val=new String[utilitaire.size()];
        for (int i = 0; i < val.length ; i++)
        {
            val[i]=request.getParameter(formName[i]);
        }
        out.print("size : "+utilitaire.size());
        try {
            String uri = utilitaire.retrieveUrlFromRawurl(request.getRequestURI());
            HashMap<String, Method> map = (HashMap<String, Method>) getServletContext().getAttribute("map");
            ViewModel model = utilitaire.invokation(path, uri, map,formName,val) ;
            if (model != null)
            {
                request.setAttribute("data", model.getData());
                request.getRequestDispatcher(model.getJsp()).forward(request,response);
            }
        } catch (Exception ex) {
            //peut-être il aura des exception car model.getJsp retourn null
            out.println("exception"+ex.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse  response) throws ServletException, IOException {
        processRequest(request,response);
    }
}
